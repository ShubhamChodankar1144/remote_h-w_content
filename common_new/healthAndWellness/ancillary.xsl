<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="supportXml" select="document('healthInfoSupport.xml')"/>
	<xsl:variable name="videoXml" select="document('../../member/enhancedServices/videos/videoData.xml')"/>
	<!-- key declarations -->
	<xsl:key name="kSupportWorkshop" match="workshop" use="topic/@directory"/>
	<xsl:key name="kSupportClass" match="class" use="topic/@directory"/>
	<xsl:key name="kSupportVideo" match="video" use="topic"/>
	<xsl:key name="kSupportGuideline" match="guideline" use="topic/@directory"/>
	<xsl:key name="kSupportService" match="service" use="topic/@directory"/>
	<xsl:key name="kSupportSection" match="section" use="topic/@directory"/>
	<xsl:key name="kSupportArticleNW" match="article[@type='NORTHWEST HEALTH MAGAZINE']" use="topic/@directory"/>
	<xsl:key name="kSupportArticleV" match="article[@type='VITALITY MAGAZINE']" use="topic/@directory"/>
	<xsl:key name="kSupportArticlePS" match="article[@type='PATIENT STORIES']" use="topic/@directory"/>
	<xsl:key name="kSupportPicture" match="picture" use="topic/@directory"/>
  <xsl:param name="env" /> 

	<xsl:template name="rightAncillary">
		<xsl:param name="pConsumerType" select="'notPassed'"/> 
		
		<xsl:variable name="vURL">
		  <xsl:choose>
			<xsl:when test="$env='development'">https://member-dev.ghc.org/enhancedServices/videos/</xsl:when>
			<xsl:when test="$env='qa'">https://member-qa.ghc.org/enhancedServices/videos/</xsl:when>
			<xsl:when test="$env='production'">https://member.ghc.org/enhancedServices/videos/</xsl:when>
		  </xsl:choose>
		</xsl:variable>    
		
		<xsl:variable name="support">
		  <xsl:for-each select="$supportXml">
			<xsl:choose>
			<!-- Testing whether there is at least one piece of content for the current topic in $supportXml-->
			  <xsl:when test="key('kSupportWorkshop', $pCurTopic) or key('kSupportClass', $pCurTopic) or key('kSupportSection', $pCurTopic)">1</xsl:when>
			  <xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		  </xsl:for-each>
		</xsl:variable>
		<!-- Testing whether there is at least one piece of content for the current topic in $videoXml-->
		<xsl:variable name="video">
		  <xsl:for-each select="$videoXml">
			<xsl:choose>
			  <xsl:when test="key('kSupportVideo', $pCurTopic)">1</xsl:when>
			  <xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		  </xsl:for-each>
		</xsl:variable>
		<!-- Only create the supportRight div if we know there is content coming from at least one of $videoXml or supportXml -->
		<xsl:if test="$support=1 or $video=1">
			<div id="supportRight" class="indexRightNav">
			<h3 class="index">GROUP HEALTH RESOURCES</h3>
				<div id="supportLinks">
					<xsl:for-each select="$supportXml">
						<xsl:call-template name="generateLinks">
							<xsl:with-param name="keyName" select="'kSupportWorkshop'"/>
							<xsl:with-param name="heading" select="'WORKSHOPS'"/>
							<xsl:with-param name="path" select="//workshop"/>
						</xsl:call-template>
						<xsl:call-template name="generateLinks">
							<xsl:with-param name="keyName" select="'kSupportClass'"/>
							<xsl:with-param name="heading" select="'CLASSES AND PROGRAMS'"/>
							<xsl:with-param name="path" select="//class"/>
						</xsl:call-template>
						<xsl:call-template name="generateLinks">
							<xsl:with-param name="keyName" select="'kSupportSection'"/>
							<xsl:with-param name="heading" select="'SPECIAL SECTIONS'"/>
							<xsl:with-param name="path" select="//section"/>
						</xsl:call-template>
					</xsl:for-each>
					<xsl:for-each select="$videoXml">
						<xsl:if test="key('kSupportVideo', $pCurTopic)">
						<h5>VIDEOS</h5>
					<xsl:for-each select="//video">
						<xsl:if test="topic = $pCurTopic">
							<p class="supportLink">
								<a href="{$vURL}"><xsl:value-of select="title"/></a>
							</p>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>
					</xsl:for-each>
				</div>
			</div>
		</xsl:if>
		<xsl:call-template name="guidelines"/>
		<xsl:call-template name="services"/>
	</xsl:template>
	
	
	<xsl:template name="guidelines">
		<xsl:variable name="guides">
			<xsl:for-each select="$supportXml">
				<xsl:choose>
				<!-- Testing whether there is at least one clinical guideline for the current topic in $supportXml-->
					<xsl:when test="key('kSupportGuideline', $pCurTopic)">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="$guides=1">
			<div id="guidelines" class="indexRightNav">
				<h3 class="index">CLINICAL GUIDELINES</h3>
				<p>Your doctor and medical team may use these guidelines to make decisions and recommendations about your care.</p>
				<xsl:for-each select="$supportXml">
					<div id="supportLinks">
						<xsl:for-each select="//guideline">
							<xsl:if test="topic/@directory = $pCurTopic">
								<p class="supportLink">
									<a href="{url}"><xsl:value-of select="title"/></a> (PDF)
								</p>
							</xsl:if>
						</xsl:for-each>
						<p class="supportLink">
							<a href="?item=/common/healthAndWellness/careDecisions/yourCare/guidelines.html">See all guidelines</a>
						</p>
					</div>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>
	
	
	<xsl:template name="services">
		<xsl:variable name="service">
			<xsl:for-each select="$supportXml">
				<xsl:choose>
				<!-- Testing whether there is at least one clinical guideline for the current topic in $supportXml-->
					<xsl:when test="key('kSupportService', $pCurTopic)">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="$service=1">
			<div id="specialtyCare" class="rightText">
				<xsl:for-each select="$supportXml">
					<h5>Specialty Care at Group Health</h5>
					<p>With more than 900 doctors practicing in more than 60 specialties, Group Health has one of the largest medical groups in the state.<br/>
						<xsl:for-each select="//service">
							<xsl:if test="topic/@directory = $pCurTopic">
									<a href="{url}"><xsl:value-of select="title"/></a><br/>
							</xsl:if>
						</xsl:for-each>
					</p>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>
	
	
	<xsl:template name="bottomAncillary">
		<xsl:variable name="bottom">
			<xsl:for-each select="$supportXml">
				<xsl:choose>
					<!-- Testing whether there is at least one bottom ancillary article for the current topic in $supportXml-->
					<xsl:when test="key('kSupportArticlePS', $pCurTopic) or key('kSupportArticleV', $pCurTopic)">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="$bottom=1">
		
			<div id="bottomAncillary">
				<p class="homeLine"/>
				<xsl:for-each select="$supportXml">
					<xsl:call-template name="generateBlocks">
						<xsl:with-param name="keyName" select="'kSupportArticleV'"/>
						<xsl:with-param name="path" select="//article[@type='VITALITY MAGAZINE']"/>
						<xsl:with-param name="type" select="'VITALITY MAGAZINE'"/>
					</xsl:call-template>
					<xsl:call-template name="generateBlocks">
						<xsl:with-param name="keyName" select="'kSupportArticleNW'"/>
						<xsl:with-param name="path" select="//article[@type='NORTHWEST HEALTH MAGAZINE']"/>
						<xsl:with-param name="type" select="'NORTHWEST HEALTH MAGAZINE'"/>
					</xsl:call-template>
					<xsl:call-template name="generateBlocks">
						<xsl:with-param name="keyName" select="'kSupportArticlePS'"/>
						<xsl:with-param name="path" select="//article[@type='PATIENT STORIES']"/>
						<xsl:with-param name="type" select="'PATIENT STORIES'"/>
					</xsl:call-template>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>


	<xsl:template name="generateLinks">
		<xsl:param name="keyName"/>
		<xsl:param name="heading"/> <!-- title text -->
		<xsl:param name="path"/> <!-- content file url -->
		<xsl:if test="key($keyName, $pCurTopic)">
			<h5><xsl:value-of select="$heading"/></h5>
			<xsl:for-each select="$path">
				<xsl:if test="topic/@directory = $pCurTopic">
					<p class="supportLink">
							<a class="supportLink" href="{url}"><xsl:value-of select="title"/></a>
					</p>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	
	
	<xsl:template name="generateBlocks">
		<xsl:param name="keyName"/>
		<xsl:param name="path"/>
		<xsl:param name="type"/>
		<xsl:if test="key($keyName, $pCurTopic)">
			<h5 class="indexAncillaryHead"><xsl:value-of select="$type"/></h5>
			<xsl:for-each select="$path">
				<xsl:if test="topic/@directory = $pCurTopic and @type = $type">
					<p class="ancillaryArticle">
						<xsl:if test="image">
							<img class="ancillaryPic" src="{image/imageUrl}" alt="{image/imageAlt}"/>
						</xsl:if>
						<a href="{url}"><xsl:value-of select="title"/></a><br/><xsl:value-of select="summary"/>
					</p>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	
	
	<!-- Only create the topicPic div is we know there is a picture associated with the topic -->
		<xsl:template name="addImage">
			<xsl:variable name="picture">
				<xsl:for-each select="$supportXml">
					<xsl:choose>
						<!-- Testing whether there is a picture associated with the topic page. -->	
						<xsl:when test="key('kSupportPicture', $pCurTopic)">1</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:variable>
			<xsl:if test="$picture=1">
				<xsl:for-each select="$supportXml">
					<xsl:if test="key('kSupportPicture', $pCurTopic)">
						<div id="topicPic">
						<xsl:for-each select="//picture">
							<xsl:if test="topic/@directory = $pCurTopic">
								<img class="topicPic" src="{imageUrl}" alt="{imageAlt}"/>
							</xsl:if>
						</xsl:for-each>
						</div>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:template>
</xsl:stylesheet>
