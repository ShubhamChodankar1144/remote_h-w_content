<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="no"/>
	<xsl:template match="/"> 
			<xsl:apply-templates />	
	</xsl:template>
	<xsl:template match="program">	
				<h3 class="videoGroupHeadingList"><xsl:value-of select="@title"/></h3>
					<ul class="videoList">
						<xsl:for-each select="video">
							<li class="plainVideo">
								<xsl:value-of select="title"/>
							</li>
						</xsl:for-each>
					</ul>		
	</xsl:template>
</xsl:stylesheet>