﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:include href="ancillary.xsl"/>
	
	<!-- the templates to be used to render content; defined as parameters in case we ever want to pass this into the stylesheet -->
	<xsl:param name="pConsumerType" select="'notPassed'"/>
	<xsl:param name="topic" select="'notPassed'"/> <!-- passed from query, used to render topic indexes -->
	<xsl:param name="pCurCat" select="substring-before($topic, '/')"/>
	<xsl:param name="pCurTopic" select="substring-after($topic, '/')"/>
	<!-- global parameters used in this stylesheet -->
	<xsl:param name="pHlthInfoSecLoc" select="'/public/healthAndWellness/index.jhtml'"/> <!-- location of health_info section -->
	<xsl:param name="pHlthInfoArtLoc" select="'/common/healthAndWellness/'"/> <!-- location of health_info articles -->
	<xsl:param name="pKbasePage" select="'/kbase/topic.jhtml?docId='"/> <!-- location of kbase render page -->

	<xsl:template match="/">
	<link href="/all-sites/ssi/index.css" rel="stylesheet" type="text/css" />
			<xsl:choose>
				<xsl:when test="$topic = 'notPassed' or $topic = '/'">
					<div id="ghContentMain" class="index">
					<!-- topic not passed to the page. render as main index -->
					<h1 class="index">Health and Wellness Resources</h1>
					<div id="ghCenterCol" class="index">
					<div id="menu" class="menu_list index">
						<xsl:apply-templates/>
					</div>
					<xsl:call-template name="bottomContent"/>
					</div>
					</div>
					<div id="ghRightCol">
						<div class="promoContainer">
							<a href="/kbase/index.jhtml">Healthwise® Knowledgebase</a><br/><br/>
							<a href="https://www1.ghc.org/html/public/customer-service/resource-line.html">Resource Line</a><br/><br/>
							<a href="https://www1.ghc.org/html/public/services/consulting-nurse.html">Consulting Nurse Service</a><br/><br/>
							<a href="/public/classesAndEvents/">Classes &amp; Events</a><br/>
						</div>
						<xsl:call-template name="rightSidebarProfile"/>
						<xsl:call-template name="rightSidebarMain"/>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div id="ghContentMain" class="index">
				<!-- render topic index -->
						<div id="ghCenterCol" class="index">
							<div id="topicTop">	
								<xsl:apply-templates/>
							</div>
							<xsl:call-template name="bottomAncillary"/>
						</div>
					</div>
					<div id="ghRightCol">
						<div class="promoContainer">
							<xsl:choose>
								<xsl:when test="$topic = 'conditions/coldsAndFlu'">
									<a href="/public/customerservice/index.jhtml">Customer Service</a>
								</xsl:when>
								<xsl:otherwise>
									<a href="https://www1.ghc.org/html/public/customer-service/resource-line.html">Resource Line</a>
								</xsl:otherwise>								
							</xsl:choose>
							<br/><br/>
							<a href="https://www1.ghc.org/html/public/services/consulting-nurse.html">Consulting Nurse Service</a><br/>
						</div>	
						<xsl:call-template name="rightAncillary">
							<xsl:with-param name="pConsumerType" select="$pConsumerType"/>
						</xsl:call-template>
					</div>
				</xsl:otherwise>
			</xsl:choose>

		<script type="text/javascript" src="/all-sites/ssi/scripts/healthAndWellness.js"> </script>
	</xsl:template>
	
	<!-- ignore these elements !!!-->
	<xsl:template match="topickeywords | topicdescription | topictitle | name | directory | filename"/>
	
							
	<xsl:template match="category">								
		<xsl:choose>
			<xsl:when test="$topic = 'notPassed' or $topic = '/'">
				<!-- topic not passed to the page. render as main index -->
				<head>
					<title>Health and Wellness Resources</title>
					<meta name="description" content="This section has extensive articles on fitness, nutrition, conditions, diseases, and preventive care.  You'll find classes, videos about treatment options, and interactive tools. Categories include children, teens, women, men, and healthy aging."/>
					<meta name="keywords" content="health information, health and wellness, health, wellness, health resources, health topics, symptoms, allergies, arthritis, asthma, back problems, bone, joint, muscle, pain, injury, cancer, colds, flu, COPD, pulmonary, depression, diabetes, eye, vision, headache, migraine, heart, coronary, cardiac interactive tool, hypertension, blood pressure, cholesterol, respiratory, lung, STD, sexually transmitted, sinus, skin, acne, sleep, stomach, digestive, antibiotic, medicine, meds, drugs, medication, vitamin, exam, check up, checkup, visit, physical, recommendations, immunization, vaccine, shots, pregnancy, labor and birth, delivery, newborn, infant, baby, child, children, kids, well care, well-care, preventive, preventative, well adult, well-adult, teen, teenager, teen-ager, Totally Teen, magazine, Healthwise, parent, parenting, men, women, adult, senior, older, aging,  healthy aging, fitness, activity, exercise, moving, nutrition, healthy, food, eat, lifestyle, wills, advance directives, care, library, video, DVD, class, program, workshop, clinical guidelines, Northwest Health, health profile"/>	
				</head>
				<h4 class="menu_head index" id="{directory}" name="{directory}">
					<xsl:value-of select="name"/>
				</h4>
				<ul class="menu_body index">
					<xsl:apply-templates/>
				</ul>
			</xsl:when>
			<xsl:otherwise> 			
				<!-- rendering a topic index, go find the requested topic -->
				<xsl:apply-templates select="topic[ancestor::category/directory=$pCurCat and directory=$pCurTopic]"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
						
							
	<xsl:template match="topic"> 				 					
		<xsl:choose>
			<xsl:when test="$topic = 'notPassed' or $topic = '/'">
				<!-- for main index, render a link to this topic's index -->		
				<xsl:variable name="vIndexUrl">
					<!-- create URL to the topic index -->
					<xsl:choose>
						<xsl:when test="@type='kbaseIndex' or @type='specialArticle'">
						<!-- a topic with type="kbaseIndex" is a link to a Healthwise topic index; a topic with type='specialArticle' links directly to an article page; in either case its directory element will contain the URL -->
							<xsl:value-of select="directory"/>
						</xsl:when>
						<xsl:otherwise>
						<!-- otherwise assume we are building link to a Health and Wellness topic index -->
							<xsl:value-of select="concat($pHlthInfoSecLoc, '?topic=', ancestor::category/directory, '/', directory)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<li>
					<a href="{$vIndexUrl}">
						<xsl:value-of select="name"/>
					</a>
				</li>
				<!-- we are done rendering main index, no need to apply-templates -->
			</xsl:when>
			<xsl:otherwise>
				<!-- for topic index render topic name as heading -->
				<h1 class="index">
					<xsl:value-of select="name"/>
					<title><xsl:value-of select="topictitle"/></title>						    
					<xsl:element name="meta">
					        <xsl:attribute name="name">description</xsl:attribute>
					        <xsl:attribute name="content"><xsl:value-of select="topicdescription"/></xsl:attribute>
				    </xsl:element>							    							 
					<xsl:element name="meta">
					        <xsl:attribute name="name">keywords</xsl:attribute>
					        <xsl:attribute name="content"><xsl:value-of select="topickeywords"/></xsl:attribute>
				    </xsl:element>		
				</h1>
				<xsl:call-template name="addImage"/>
				<xsl:choose>
					<xsl:when test="directory = 'diabetes'">
					<span class="anchorBlockHead">ON THIS INDEX:</span>
						<div id="anchorBlock">
							<div id="leftColumn">
								<a href="#Basics">Basics</a><br/>
								<a href="#Exams and Tests">Exams and Tests</a><br/>
								<a href="#Blood Sugar Control">Blood Sugar Control</a><br/>
								<a href="#Medicines">Medicines</a><br/>
								<a href="#Exercise">Exercise</a><br/>
							</div>
							<div id="rightColumn">
								<a href="#Nutrition and Meal Planning">Nutrition</a><br/>
								<a href="#Stress and Feelings About Diabetes">Stress and Feelings</a><br/>
								<a href="#Complications From Diabetes">Complications</a><br/>
								<a href="#Diabetes During Pregnancy">Diabetes in Pregnancy</a><br/>
								<a href="#Children With Diabetes">Children With Diabetes</a><br/>
							</div>
						</div>
					</xsl:when>
				</xsl:choose>
				<!--<xsl:call-template name="addImage"/>-->
				<ul class="index">
					<xsl:apply-templates/>
				</ul>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="subtopic">
		<li class="index">
			<strong><a class="noHover" name="{name}">
				<xsl:value-of select="name"/></a>
			</strong>
			<ul>
				<xsl:apply-templates/>
			</ul>
		</li>
	</xsl:template>

	
	<xsl:template match="article">
	<!--	**** build the href for the article link ****
				the href will vary based on the "type" attribute of the article
				the default href assumes a Health and Wellness article within the current category/topic
				@type='kbase' is for Healthwise pages, which are rendered in the kbase/topic.jhtml template
				@type='other' is for articles that are cross-linked (article is not in the same category/topic as the index on which it appears)
				@type='other' is also for links to Health and Wellness topic index pages 
				@type='other' is also for non Health and Wellness links
		-->
		<xsl:variable name="vLinkHref">
			<xsl:choose>
				<xsl:when test="@type and @type='kbase'">
				<!-- kbase topic articles are rendered by the kbase page -->
					<xsl:value-of select="concat($pKbasePage, filename)"/>
				</xsl:when>
				<xsl:when test="@type and @type='other'">
				<!--	links of type "other" must have a valid relative URL as their filename 
						*unless* it is a Health and Wellness article, in which case we pass into render.jhtml-->
					<xsl:choose>
						<xsl:when test="starts-with(filename, '/common/healthAndWellness')">
							<xsl:value-of select="concat($pHlthInfoSecLoc, '?item=', filename)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="filename"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
				<!-- otherwise, assume the link is to a Health and Wellness article in its proper location in the index.xml file -->
					<xsl:value-of select="concat($pHlthInfoSecLoc, '?item=', $pHlthInfoArtLoc, ancestor::category/directory, '/', ancestor::topic/directory, '/', filename)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="vPdf">
			<xsl:choose>
				<xsl:when test="contains(filename, 'pdf')"> (PDF)
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:variable>
		<li>
			<a href="{$vLinkHref}">
				<xsl:value-of select="title"/>
			</a>
			<xsl:value-of select="$vPdf"/>
		</li>
	</xsl:template>
		
	<xsl:template name="bottomContent">
		<p class="bottomAncillary subhead">Seasonal and Hot Topics</p>
		
		<p class="ancillaryArticle">
		<a href="https://www.ghc.org/kbase/topic.jhtml?docId=hw222891">Know the Signs of a Stroke</a>
		<img alt="flower" src="/all-sites/images/healthAndWellness/headache-sm.jpg" class="ancillaryPic" />
		<br/>May is National Stroke Awareness Month. If someone close to you suffers a stroke, it's critical to recognize the signs and get medical help quickly to lessen damage. <a href="http://www.strokeassociation.org/STROKEORG/WarningSigns/Stroke-Warning-Signs-and-Symptoms_UCM_308528_SubHomePage.jsp?gclid=CMTrjvKkxswCFUKUfgodEDgJvgUse">Remember with the "FAST" method</a>.</p>
	
		<p class="ancillaryArticle">
		<a href="https://www1.ghc.org/html/public/vitality/20160301-positive">10 Ways to a Happier, Positive Mood</a>
		<img alt="flower" src="/all-sites/images/healthAndWellness/flower-sm.jpg" class="ancillaryPic" />
		<br/><a href="https://www1.ghc.org/html/public/vitality/">Vitality</a>, our healthy aging newsletter, covers positive thinking, balance exercises, and teaming up with your doctor for better health.</p>
		
		<p class="ancillaryArticle">
		<a href="https://www1.ghc.org/html/public/health-wellness/alert/zika">Zika Virus Disease</a>
		<br/>Answers to common questions about this disease that can cause birth defects.</p>
		
	
		<p class="ancillaryArticle">
		<a href="https://www1.ghc.org/html/public/health-wellness/sugimoto/hep-c">New Treatment for Hepatitis C</a>
		<br/>Ginny Sugimoto, MD, a Group Health family medicine physician, talks about testing and the 12-week treatment regimen.</p>
		
		<p class="ancillaryArticle">
		<img alt="man and woman" src="/all-sites/images/healthAndWellness/smiling-couple.jpg" class="ancillaryPic" />
		<a href="/public/classesAndEvents/class.jhtml?reposid=/common/news/ongoing/lwccworkshops.html&amp;type=ongoing&amp;region=2">Check Out Our Living Well Programs</a>
		<br/>These programs help people with diabetes, asthma, arthritis, and other ongoing conditions manage their health and improve their quality of life.  Join an online workshop now to help you stay on track in 2016!</p>
		
		<p class="ancillaryArticle">
		<a href="https://www1.ghc.org/html/public/health-wellness/riggs/bp-1">Blood Pressure Goals Are Evolving</a>
		<br/>Bob Rigs, MD, a Group Health family medicine physician, discusses new studies and blood pressure goals.</p>


		
				
		<!--
		<p class="ancillaryArticle">
		<a href="/public/healthAndWellness/?item=/common/healthAndWellness/adult/seniorHealth/falls.html">Improve Balance to Lower the Danger of Falling</a>
		<img alt="exercise" src="/all-sites/images/healthAndWellness/senior-exercise-sm.jpg" class="ancillaryPic" />
		<br/>Walking, balance exercises, and simple precautions help prevent injury as we get older.</p>
		<p class="ancillaryArticle">
		<a href="https://www1.ghc.org/html/public/health-wellness/sugimoto/antibiotics.html">Antibiotic Overuse and the Rise of Superbugs</a>
		<img alt="phone screen" src="/all-sites/images/healthAndWellness/pill-bottle-small.jpg" class="ancillaryPic" />
		<br/>Don't expect antibiotics to be the right medicine for common winter illness.  When antibiotics are overused, they create superbugs that are harder to treat.</p>
		<p class="ancillaryArticle">
		<a href="https://www1.ghc.org/html/public/promo/wisely">Lung Cancer Screening</a>
		<img alt="exercise" src="/all-sites/images/healthAndWellness/decision-sm.jpg" class="ancillaryPic" />
		<br/>Group Health’s Choosing Wisely campaign, in partnership with the ABIM Foundation and Consumer Reports, explains that screening is only helpful for high-risk people, but has risks.</p>
		<p class="ancillaryArticle">
		<img alt="man with e-cigarette" src="/all-sites/images/healthAndWellness/smoke-sm.jpg" class="ancillaryPic" />
		<a href="https://www1.ghc.org/html/public/health-wellness/sugimoto/e-cigarettes.html">E-Cigarettes, Vaping, and Hookahs</a>
		<br/>These alternative methods for consuming tobacco are still harmful to health.</p>
		
				<p class="ancillaryArticle">
		<a href="https://www1.ghc.org/html/public/promo/wisely">Choosing Wisely: Children and Antibiotics</a>
		<img alt="runny nose kid" src="/all-sites/images/healthAndWellness/sick-kid-sm.jpg" class="ancillaryPic" />	
		<br/>In May, Group Health’s Choosing Wisely campaign, in partnership with the ABIM Foundation and Consumer Reports, looks at when children need antibiotics for a sore throat, cough, or runny nose.</p>
				
				
				<p class="ancillaryArticle">
		<a href="/public/healthAndWellness/?item=/common/healthAndWellness/conditions/coldsAndFlu/vaccineStatus.html">Get Your Flu Vaccination</a> 
		<br/>Flu vaccinations are available at all Group Health Medical Centers locations.<br /></p>
		
		<p class="ancillaryArticle">
		<img alt="Coughs, Colds and Flu Tool" src="/all-sites/images/healthAndWellness/cold-flu-small.jpg" class="ancillaryPic" />
		<a href="/public/tools/coldsAndFlu/">Do You Have a Cold or the Flu?</a> 
		<br/>Check your symptoms and get home care advice.  Find out if you should see your doctor.</p>
		
		<p class="ancillaryArticle">
		<a href="http://consumerhealthchoices.org/grouphealth/ ">Choosing Wisely</a> 
		<br/>These short articles describe the pros and cons of some common tests and treatments. From the national Choosing Wisely campaign, led by Consumer Reports.</p>
		
		<p class="ancillaryArticle">
		<img alt="doctor" src="/all-sites/images/healthAndWellness/decision-sm.jpg" class="ancillaryPic" />
		<a href="/public/healthAndWellness/?item=/common/healthAndWellness/careDecisions/yourCare/decisionTools.html">Considering Your Care Options? These Tools Can Help</a> 
		<br/>Our tools can help you have a better discussion with your doctor. Each month, the Choosing Wisely campaign looks at when a common test and treatment is appropriate.<br />
		<a href="http://consumerhealthchoices.org/wp-content/uploads/2012/08/ChoosingWiselyPapTestsAAFP-ER.pdf">This Month's Feature: Pap Test Facts</a></p>
		
		<p class="ancillaryArticle">
		<img alt="runner" src="/all-sites/images/healthAndWellness/fitness-small.png" class="ancillaryPic" />
		<a href="https://www1.ghc.org/html/public/about/events">Spring and Summer Fitness Events</a> 
		<br/>Group Health sponsors walking, running, and cycling events.  See what is coming up in your area.<br />
		
		
		<p class="ancillaryArticle">
		<img alt="girl" src="/all-sites/images/healthAndWellness/backpack.jpg" class="ancillaryPic" />
		<a href="http://www.ghc.org/kbase/topic.jhtml?docId=abk0958">Don't Overload Your Child's Backpack</a> 
		<br/>As school starts, check out these backpack tips to help prevent common injuries.</p>
	
		
			
		<p class="ancillaryArticle">
		<a href="/public/healthAndWellness/?item=/common/healthAndWellness/conditions/coldsAndFlu/vaccineStatus.html">Get Your Flu Vaccination</a> 
		<br/>Flu vaccinations are available at all Group Health Medical Centers locations.<br /></p>		
		<p class="ancillaryArticle">
				<img alt="want to sleep better?" src="https://asset.ghc.org/ghc-resources/images/public/nwhealth/20130317-sleep-small.jpg" class="ancillaryPic" />
		<a href="https://www1.ghc.org/html/public/features/20130317-sleep.html">Want to Sleep Better?</a> 
		<br/>Group Health experts can help you understand what's behind your restless nights, and how to improve your sleep.</p>
		
		
		<p class="ancillaryArticle">
		<a href="/public/news/20120216-pertussis.jhtml">Drop By Our Clinics for Whooping Cough Vaccine</a>
		<br/>As friends and family gather for the holidays, make sure infants are protected from this highly contagious disease.  Get a whooping cough vaccination when you come in for a flu shot.</p> 
		
		
		<p class="ancillaryArticle">
		<a href="/kbase/topic.jhtml?docId=/tn7285/tn7296.xml">Finding the Right Shoes</a> 
		<br/>Ready to walk more as the weather gets nicer?  How to select the right footwear, even with problem feet.</p>
		
		<p class="ancillaryArticle">
		<a href="/public/healthAndWellness/?item=/common/healthAndWellness/careDecisions/yourCare/reminders.html">Preventive-Care Outreach</a> 
		<br/>Group Health uses letters and phone calls to remind members about preventive-care visits.</p>
		
		
		<p class="ancillaryArticle">
		<img alt="Chris Fordyce" src="/all-sites/images/healthAndWellness/fordyceSmall.jpg" class="ancillaryPic" />
		<a href="/public/healthAndWellness/?item=/common/healthAndWellness/adult/seniorHealth/videos.html">Videos for Healthy Aging</a>
		<br/>Dr. Chris Fordyce, Group Health's Medical Director for Healthy Aging, talks about preventive care and demonstrates simple exercises for balance and flexibility.</p>
		
		<p class="ancillaryArticle">
		<img alt="man and woman" src="/public/tools/heart/smallCardiacTool.jpg" class="ancillaryPic" />
		<a href="/public/tools/heart/index.jhtml">What's Your Heart Disease Risk?</a>
		<br/>Use our new Cardiac Risk Calculator to find your risk of heart disease, stroke, or other cardiovascular disease in the next five years.  Learn how to reduce your risk.</p> --> 
	</xsl:template>


	<xsl:template name="rightSidebarMain">
		<div class="indexRightNav">
			<h3>VIDEOS &#38; TOOLS: DECISIONS</h3>             
			<p>Use these as you consider pros and cons about your care or treatment options.  They may help you have a better discussion with your doctor.<br/>
			<a href="/public/healthAndWellness/?item=/common/healthAndWellness/careDecisions/yourCare/decisionTools.html">Videos and Tools: Making Decisions About Your Care</a></p>
		</div>    
	
		<div class="indexRightNav">
			<h3>ROUTINE EXAMS &amp; VACCINES</h3>
			<p>Check out Group Health's recommendations on preventive care visits, screenings, and immunizations.<br/>
	<a href="/public/healthAndWellness/?item=/common/healthAndWellness/tests/recommendedTests/adultTests.html">Adult Well-Care Visits</a><br/>
	<a href="/public/healthAndWellness/?item=/common/healthAndWellness/children/childVisits/overview.html">Well-Child Visits</a></p>
		</div>
	
		<div class="indexRightNav">
			<h3>CLINICAL GUIDELINES</h3>
			<p>Your doctor and medical team may use these guidelines to make recommendations and decisions about your care.<br/>
				<a href="?item=/common/healthAndWellness/careDecisions/yourCare/guidelines.html">See all guidelines</a>
			</p>                  
		</div>
	</xsl:template>
	
	
	<xsl:template name="rightSidebarProfile">
		<br/>
		<div class="rightText">
			<h5>See Your Health Profile</h5>
			<p><img id="healthProfileImg" src="/public/images/healthProfile.JPG" alt="Health Profile"/>Get a jump on better health with an online report that assesses your risks and offers ways to improve your health.<br/> 
			<a href="/public/momentum/index.jhtml">Health Profile</a>
			</p>
		</div>
	</xsl:template>
</xsl:stylesheet>


